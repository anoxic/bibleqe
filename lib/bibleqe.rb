class BibleQE
  class Error < RuntimeError
  end

  class SearchError < Error
  end

  class ResultError < Error
  end
end
