Usage:

    $ ./bqe <search_term> [<search_term>] [:page <number>] [:limit <number>]


IMPLEMENTED ARGUMENTS

Arguments not starting in : are used as search terms (in an AND search)

<term>|<term> executes an OR search

/<regex>/ for a regular expression search

:page <num> sets which page number to view (default = 1)

:limit <num> sets how many results per page (default = 10)

:all will display all results

:show (on by default) show result text

:list list references

:text <version> Change text  (by version abbreviation)


UNIMPLEMENTED ARGUMENTS

<term*> (any term with an *) - wildcard search

!<term> - exclude verses matching this word

:in <book_or_chapter> define a scope for searching

:within <num> [words/verses] when used with two or more words, allows you to find matches for words within a certain distance of eachother


Follow guidelines in http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05 for style on commandline args

Intersting: http://about.esvbible.org/uncategorized/technical-introduction-to-the-esv-online-edition/


TODO

[ ] Add a Result.pages
[ ] Move actual querying from Result.get to Search.query

Towards a web release:
[ ] Use full length book names
[ ] Fetch by book and verse
[ ] Create a new parser that uses regex scanning
[ ] Display info about src of Bible text
[ ] Add query query log containing terms and timestamp
[ ] Create list of unsearchable words
[ ] Simpler wildcard search

Search spead:
[ ] Searching is too slow: Binary search

Finished:
[x] Use a multi-line input, with one verse per line.
[x] Create an index (as an array first) capturing word frequencies
[x] In the index, catch the line number + word number (in verse)
[x] Ignore comment lines when creating index
[x] Use a "label" in the source on each line that tells the book, 
    verse, chapter (ignore label in index)
[x] Basic search functions
[x] Allow sidenotes (marked with >) and headings (marked with #) in the text
[x] Handle the "Found x matches for ..." line
[x] Handle results with no matches
[x] Multi-word search
[x] Allow commandline parameters for search
[x] Full text name set in the text
[x] Add full KJV text
[x] Allow ignored characters (i.e. .,:;()[]{}?!) to be set in the text
[x] Make full specification
[x] Printing is too slow: Instead of loading entire file, 
    load from stream, skipping lines
[x] Searching is too slow: Start within the context of the first letter
[x] Paginate results vs. printing all
[x] Feature: List result references vs. whole verse
[x] Create a search object for querying
[x] Return int for Result.matches
[x] Return key-value hash for Result.show
[x] Format the verses better
[x] Web gateway
[x] Fix word highlighting
[x] Make search box look right without box-model: border-box
[x] Add footer with links 
